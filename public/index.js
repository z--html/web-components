const MIME_TYPES = {
  'jpeg': 'image/jpeg',
  'jpg': 'image/jpeg',
  'png': 'image/png',
  'svg': 'image/svg+xml',
  'webp': 'image/webp',
};

class ZImg extends HTMLElement {

  constructor() {
    super();

    this.alt = '';

    this.srcdir = '.';
    this.srcname = 'img';
    this.srcext = 'svg';

    this.height = '40';
    this.width = '40';
  }

  static get observedAttributes() {
    return [
      'alt',

      'srcdir',
      'srcname',
      'srcext',

      'height',
      'width'
    ];
  }

  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return;
    this[ property ] = newValue;
  }

  connectedCallback() {
    const
      shadow = this.attachShadow({ mode: 'open' }),

      ziAlt = this.alt || this.srcname,

      ziSrcDir = this.srcdir,
      ziSrcName = this.srcname,
      ziSrcExt = this.srcext,

      ziHeight = this.height,
      ziWidth = this.width;

    shadow.innerHTML = `
      <style>
        img,
        picture,
        source {
          border-radius: 50%;
          padding: 0;
          margin: 0;

          height: ${ziHeight}px;
          width: ${ziWidth}px;
        }

        picture {
          display: inline-flex;
          filter: drop-shadow(.35rem .35rem .4rem rgba(0,0,0,.5));
        }
      </style>
    `;


    const sources = [];

    ziSrcExt.split().reverse().forEach(
      ext => {
        const
        fileName = `${ziSrcDir}/${ziSrcName}.${ext}`,
        typeName = MIME_TYPES[ziSrcExt];

        typeName && sources.unshift(`<source srcset="${fileName}" type="${typeName}"/>`);
      }
    );

    sources.push(`<img alt="${ziAlt}" src="${ziSrcDir}/${ziSrcName}.${ziSrcExt[-1]}" height="${ziHeight}" width="${ziWidth}"/>`);

    const container = document.createElement('picture');
    container.innerHTML = sources.join('');

    shadow.append(container);
  }
}
customElements.define( 'z-img', ZImg );


// ---


class ZPhrase extends HTMLElement {

  constructor() {
    super();
    this.name = '';
    this.outlook = '';
  }

  static get observedAttributes() {
    return ['name', 'outlook'];
  }

  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return;
    this[ property ] = newValue;
  }

  connectedCallback() {
    const
      shadow = this.attachShadow({ mode: 'closed' }),

      template = document.getElementById('z-phrase').content.cloneNode(true),

      zpName = this.name,
      zpOutlook = this.outlook;

    Array.from( template.querySelectorAll('.zp-name') )
      .forEach( n => n.textContent = zpName );

    Array.from( template.querySelectorAll('[name="zp-outlook"]') )
      .forEach( n => n.classList.add(zpOutlook) );

    shadow.append( template );
  }
}
customElements.define( 'z-phrase', ZPhrase );
